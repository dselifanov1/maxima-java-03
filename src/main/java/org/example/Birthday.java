package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Birthday {




    public static int getAge(int year, int month, int date) {

        LocalDate date1 = LocalDate.of (year, month, date);

        LocalDate date2 = LocalDate.now ();

        int age = (int) ChronoUnit.DAYS.between(date1, date2);


        return age;
    }

     public static LocalDate nextBirthday (int year, int month, int date){


         int age = Birthday.getAge(year, month, date);

         int dateBirthday = 1000 - age % 1000;

         LocalDate date4 = LocalDate.now ().plus (dateBirthday, ChronoUnit.DAYS);


         return date4;
    }


}
