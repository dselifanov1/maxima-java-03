package org.example;

import java.util.ArrayList;

public class QueueKitchen <T> implements AnimalKitchen<T>{

    ArrayList<T> animals = new ArrayList<>();

    public ArrayList<T> getAnimals() {
        return animals;
    }

    @Override
    public void add(T animal) {
        getAnimals().add(animal);

        System.out.println(getAnimals());
    }

    @Override
    public void feed() {

        getAnimals().remove(0);
        System.out.println(getAnimals());

    }

}