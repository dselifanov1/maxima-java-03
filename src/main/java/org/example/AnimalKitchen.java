package org.example;

public interface AnimalKitchen <T> {

    public void add(T animal);

    public void feed();

}