package org.example;


public class CatFactory {


    public static Cat createCat(String name, int weight) throws IncorrectCatWeightException {

        try {
            Cat barsik = new Cat(name, weight, false);
            System.out.println(name+ " "+ weight+" "+ barsik.isAngry());

            return barsik;    }

        catch (IncorrectCatWeightException ex) {
            Cat murzik=new Cat("Мурзик", 5, true);
            System.out.println(murzik.getName()+ " "+ murzik.getWeight()+" "+ murzik.isAngry());
            return murzik;
        }
    }

}