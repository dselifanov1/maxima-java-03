package org.example;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class StreamTransformer implements Transformable {

    public void transform(String fileIn, String fileOut) throws IOException {
        FileInputStream strR = new FileInputStream( fileIn);
        FileOutputStream strW = new FileOutputStream(fileOut);
        StringBuilder result= new StringBuilder();

        String catIsAngry;
        String a;
        int i;
        try {
            int r;
            do {
                r = strR.read();
                result.append((char) r);
            } while(r != -1);
            String text = String.valueOf(result);

            String [] cats = text.split("\n");
           for (i=0; i<cats.length; i++) {
                String [] cats1 = cats[i].split(";");

                String angry = cats1[2].replace(" ", "");

                boolean b = Boolean.parseBoolean(angry);

                catIsAngry = (b == true?  "Angry" :  "Friendly");
                String name = cats1[0];
                String weight = cats1[1];

                String catFormat = String.format("%s cat %s has weight%s kg \n", catIsAngry, name, weight);
                System.out.println(catFormat);

                byte[] buffer = catFormat.getBytes(StandardCharsets.UTF_8);
                strW.write(buffer, 0, buffer.length);

            }

        }
        catch (IOException e){
            e.printStackTrace();
        }


    }
}
