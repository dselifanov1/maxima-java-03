package org.example;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CatStatistics {



    public static ArrayList <Cat> sortByNameAscending (ArrayList <Cat> cats) { //возвращает список котов, отсортировав
                                                                               // по имени по возрастанию.

        return cats.stream()
                .sorted((cat1, cat2) -> cat1.getName().compareTo(cat2.getName()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<Cat> sortByWeightDescending(ArrayList<Cat> cats) { //Возвращает список котов, отсортировав по
                                                                               // убыванию веса

        return cats.stream()
                .sorted((cat1, cat2) -> cat2.getWeight() - cat1.getWeight())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<Cat> removeFirstAndLast(ArrayList<Cat> cats) {     //возвращает список котов кроме первого и последнего.

        return cats.stream()

                .skip( 1)
                .limit(cats.size()-2)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public static Cat findFirstNonAngryCat(ArrayList<Cat> cats) {           //возвращает первого не-сердитого кота в списке.

        return cats.stream()

                .dropWhile(Cat::isAngry)
                .limit(1)
                .findFirst().orElse(null);
    }
    static int getCommonWeight(ArrayList<Cat> cats, boolean onlyAngry){     //возвращает суммарный вес всех котов (если параметр
                                                                          // onlyAngry равен true, то только сердитых котов).


        int result;
        Stream<Cat> caty = onlyAngry ? cats.stream().filter(Cat::isAngry) : cats.stream();
        result = caty.map(Cat::getWeight).reduce(Integer::sum).orElse(0);

        /*if (onlyAngry) {
            result = cats.stream()
                    .filter(Cat::isAngry)
                    .map(Cat::getWeight)
                    .reduce(Integer::sum).orElse(0);
        }
        else {result = cats.stream()
                .map(Cat::getWeight)
                .reduce(Integer::sum).orElse(0);}*/
        return result;
    }
    public static Map<String, List<Cat>> groupCatsByFirstLetter (ArrayList<Cat> cats){     //возвращает список котов сгруппировав их по
                                                                                 // первой букве имени и отсортировав группировку по возрастанию.
        return   cats.stream()
                .collect(Collectors.groupingBy(s -> s.getName().substring(0,1)));


    }
}
